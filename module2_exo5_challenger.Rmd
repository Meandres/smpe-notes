---
title: "Challenger shuttle"
author: "Ilya Meignan--Masson"
date: "2022-10-17"
output: html_document
---
### Création du vecteur des données

```{r include=FALSE}
library(tidyverse)
```


```{r}
data <- read.csv("shuttle.csv",header=T)
summary(data)

```
### Filtre seulement les vols où il y a eu un problème
```{r }
pb <- data %>% filter(Malfunction>0)
pb

```

### Analyse de l'influence de la température sur la probabilité d'avoir un problème
```{r }
ggplot(pb, aes(x=Temperature, y=Malfunction/Count)
       ) +
  geom_point()+
  coord_cartesian(ylim=c(0,1))+
  theme_bw()
```
```{r }
logistic_reg = glm(data=pb, Malfunction/Count ~ Temperature, weights=Count, 
                   family=binomial(link='logit'))
summary(logistic_reg)
```

Essai de combinaison des deux derniers blocs

```{r }
ggplot(pb, aes(x=Temperature, y=Malfunction/Count)
       ) +
  geom_point()+
  coord_cartesian(ylim=c(0,1))+
  theme_bw() +
  geom_smooth(method = "glm",
              method.args = list(family = "binomial"), fullrange = TRUE )
```
