---
title: "Is foot size related to orthographic mistakes"
author: "Ilya Meignan--Masson"
date: "2022-10-17"
output: html_document
---

```{r, include=FALSE}
library(tidyverse)
```

Read the csv file and print summary
```{r}
data <- read_csv(file = "foot_size_data.csv")
summary(data)
```
```{r}
ggplot(data, aes(x=feet_size, y=nb_mistakes))+
  geom_point()+
  theme_bw()
```
```{r}
data_averages <- data %>% group_by(feet_size) %>% summarize(avg_nb_mistakes=mean(nb_mistakes))
show(data_averages)
```

```{r}
ggplot(data, aes(x=feet_size, y=nb_mistakes))+
  geom_point()+
  geom_line(data = data_averages, aes(x=feet_size, y=avg_nb_mistakes), color="red")+
  theme_bw()
```